﻿using Gym.Service.DataService;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Gym.Service
{
    public class CreateMembershipCommand: IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class CreateMembershipCommandHandler : AsyncRequestHandler<CreateMembershipCommand>
    {
        readonly IMembershipDataService membershipDataService;
        readonly IUnitOfWork unitOfWork;

        public CreateMembershipCommandHandler(IMembershipDataService membershipDataService, IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.membershipDataService = membershipDataService;
        }

        protected async override Task Handle(CreateMembershipCommand request, CancellationToken cancellationToken)
        {
            await membershipDataService.CreateAsync(request.Name);
        }
    }
}
