﻿using Gym.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gym.Service.DataService
{
    public interface IMembershipDataService
    {
        Task CreateAsync(string name);
        Task<Membership> GetByName(string name);
        Task<IEnumerable<Membership>> GetAll();
    }
}
