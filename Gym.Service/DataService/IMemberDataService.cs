﻿using Gym.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gym.Service.DataService
{
    public interface IMemberDataService
    {
        Task AddMemberAsync(string firstName, string lastName, string email, string membereshipName);
        Task<Member> GetByEmail(string email);
        Task<IEnumerable<Member>> GetMembersAsync();
        Task<Member> GetMemberAsync(int memberId);
        Task UpdateMemberEmailAsync(int memberId, string email);
    }
}
