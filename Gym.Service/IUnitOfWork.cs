﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Gym.Service
{
    public interface IUnitOfWork
    {
        Task SaveAsync();
        void BeginTransaction();
        void Rollback();
    }
}
