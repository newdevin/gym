﻿using Gym.Service.DataService;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Gym.Service
{
    public class UpdateMemberEmailCommand : IRequest
    {
        public string Email { get; set; }
        public int MemberId { get; set; }
    }

    public class UpdateMemberEmailCommandHandler : AsyncRequestHandler<UpdateMemberEmailCommand>
    {
        private readonly IMemberDataService memberDataService;

        public UpdateMemberEmailCommandHandler(IMemberDataService memberDataService)
        {
            this.memberDataService = memberDataService;
        }
        protected async override Task Handle(UpdateMemberEmailCommand request, CancellationToken cancellationToken)
        {
            var member = await memberDataService.GetMemberAsync(request.MemberId);
            member.UpdateEmail(request.Email);
            await memberDataService.UpdateMemberEmailAsync(request.MemberId, request.Email);
        }
    }
}
