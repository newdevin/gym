﻿using Dapper;
using Gym.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Gym.Service
{
    public class GetAllMembershipsQuery : IRequest<IEnumerable<string>>
    {
        public readonly string connectionString;

        public GetAllMembershipsQuery(string connectionString)
        {
            this.connectionString = connectionString;
        }
    }

    public class GetAllMembershipsQueryHandler : IRequestHandler<GetAllMembershipsQuery, IEnumerable<string>>
    {         
        public async Task<IEnumerable<string>> Handle(GetAllMembershipsQuery request, CancellationToken cancellationToken)
        {
            using (var connection = new SqlConnection(request.connectionString))
            {
                return await connection.QueryAsync<string>("Select Name from dbo.Membership");
            }
        }
    }


}
