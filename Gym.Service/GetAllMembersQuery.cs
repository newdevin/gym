﻿using Dapper;
using Gym.Domain;
using Gym.Service.DataService;
using MediatR;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;

namespace Gym.Service
{
    public class GetAllMembersQuery : IRequest<IEnumerable<Member>>
    {
        
    }
    public class GetAllMembersQueryHandler :
        IRequestHandler<GetAllMembersQuery, IEnumerable<Member>>
    {
        private readonly IMemberDataService memberDataService;

        public GetAllMembersQueryHandler(IMemberDataService memberDataService)
        {
            this.memberDataService = memberDataService;
        }
        public async Task<IEnumerable<Member>> Handle(GetAllMembersQuery request, CancellationToken cancellationToken)
        {
            return await memberDataService.GetMembersAsync();
        }
    }
}
