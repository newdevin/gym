﻿using Gym.Service.DataService;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Gym.Service
{
    public class CreateMemberCommand : IRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MembershipName { get; set; }

    }

    public class CreateMemberCommandHandler : AsyncRequestHandler<CreateMemberCommand>
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMemberDataService memberDataService;

        public CreateMemberCommandHandler(IUnitOfWork unitOfWork, IMemberDataService memberDataService)
        {
            this.unitOfWork = unitOfWork;
            this.memberDataService = memberDataService;
        }
        protected async override Task Handle(CreateMemberCommand request, CancellationToken cancellationToken)
        {
            await memberDataService.AddMemberAsync(request.FirstName, request.LastName, request.Email, request.MembershipName);
        }
    }
}
