﻿using Gym.Data;
using Gym.Data.Entity;
using Gym.Data.Mapper;
using Gym.Service;
using Gym.Service.DataService;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Gym
{
    class Program
    {
        static readonly string connectionString = "server=.;database=Gym;trusted_connection=true;";
        static void Main(string[] args)
        {
            ServiceCollection serviceCollection = new ServiceCollection();
            ServiceProvider container = Build(serviceCollection);
            AutoMapper.Mapper.Initialize(cfg => cfg.AddProfile<MembershipProfile>());

            //GetAllMembershipsQuery command = new GetAllMembershipsQuery(connectionString);
            IMediator mediator = container.GetService<IMediator>();
            //var task = mediator.Send(command);

            //var memberships = task.Result;
            //CreateMemberCommand createMemberCommad = new CreateMemberCommand
            //{
            //    FirstName = "Devinder",
            //    LastName = "Singh",
            //    Email = "dsingh@yahoo.com",
            //    MembershipName = memberships.First()
            //};

            //var task2 = mediator.Send(createMemberCommad).Result;

            var updateMemberEMail = new UpdateMemberEmailCommand()
            {
                MemberId = 1,
                Email = "newdevin@yahoo.com"
            };
            var updated = mediator.Send(updateMemberEMail).Result;

            var getAllMembers = new GetAllMembersQuery();
            var allMembers = mediator.Send(getAllMembers).Result;

            foreach(var member in allMembers)
            {
                System.Console.WriteLine($"Email: {member.Email.email}");
            }

            System.Console.WriteLine("Email updated");
        }

        private static ServiceProvider Build(ServiceCollection serviceCollection)
        {
            serviceCollection.AddMediatR(typeof(CreateMembershipCommand));
            serviceCollection.AddTransient<IEntityMapper, EntityMapper>();
            serviceCollection.AddTransient<IMembershipDataService, MembershipRepository>();
            serviceCollection.AddDbContext<GymDbContext>(options =>
            options.UseSqlServer(connectionString),
                ServiceLifetime.Singleton);
            serviceCollection.AddTransient<IMemberDataService, MemberRepository>();
            serviceCollection.AddSingleton<IUnitOfWork, GymDbContext>((imp) => imp.GetService<GymDbContext>());

            serviceCollection.AddSingleton<IMediator, Mediator>();
            return serviceCollection.BuildServiceProvider();
        }
    }
}
