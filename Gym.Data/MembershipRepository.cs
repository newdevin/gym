﻿using Gym.Data.Entity;
using Gym.Data.Mapper;
using Gym.Domain;
using Gym.Service.DataService;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gym.Data
{
    public class MembershipRepository : IMembershipDataService
    {
        readonly GymDbContext gymDbContext;
        private readonly IEntityMapper entityMapper;

        public MembershipRepository(GymDbContext gymDbContext, IEntityMapper entityMapper)
        {
            this.gymDbContext = gymDbContext;
            this.entityMapper = entityMapper;
        }

        public async Task CreateAsync(string name)
        {
            MembershipEntity entity = new MembershipEntity { Name = name };
            gymDbContext.Add(entity);
            await gymDbContext.SaveChangesAsync();

        }

        public async Task<IEnumerable<Membership>> GetAll()
        {
            var memberships = await gymDbContext.Memberships.ToListAsync();
            return entityMapper.Map<IEnumerable<Membership>>(memberships);
        }

        public async Task<Membership> GetByName(string name)
        {
            MembershipEntity entity = await gymDbContext.Memberships
                .FirstAsync(m => m.Name == name);

            return entityMapper.Map<Membership>(entity);
        }
    }
}
