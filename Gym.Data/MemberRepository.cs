﻿using Gym.Data.Entity;
using Gym.Data.Mapper;
using Gym.Domain;
using Gym.Service.DataService;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gym.Data
{
    public class MemberRepository : IMemberDataService
    {
        private readonly GymDbContext context;
        private readonly IEntityMapper entityMapper;

        public MemberRepository(GymDbContext context, IEntityMapper entityMapper)
        {
            this.context = context;
            this.entityMapper = entityMapper;
        }
        public async Task AddMemberAsync(string firstName, string lastName, string email, string membershipName)
        {
            MembershipEntity membership = await context.Memberships.FirstAsync(m => m.Name == membershipName);
            MemberEntity member = new MemberEntity
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                RegisteredDateTime = DateTime.Now.Date,
                Membership = membership,
                MembershipId = membership.Id
            };
            context.Members.Add(member);
            await context.SaveChangesAsync();
        }

        public async Task<Member> GetByEmail(string email)
        {
            MemberEntity member = await context.Members.FirstOrDefaultAsync(m => m.Email == email);
            return entityMapper.Map<Member>(member);
        }

        public async Task<Member> GetMemberAsync(int memberId)
        {
            MemberEntity entity = await context.Members.Include(m => m.Membership)
                .FirstOrDefaultAsync(m => m.Id == memberId);

            Member member = entityMapper.Map<Member>(entity);
            //Membership membership = new Membership(entity.Membership.Id, entity.Membership.Name);
            //Member member = new Member(entity.Id, entity.FirstName, entity.LastName, entity.Email,
            //    membership, entity.RegisteredDateTime);
            return member;
        }

        public async Task<IEnumerable<Member>> GetMembersAsync()
        {
            List<MemberEntity> entities = await context.Members
                .Include(m => m.Membership)
                .ToListAsync();

            return entityMapper.Map<IEnumerable<Member>>(entities);

        }

        public async Task UpdateMemberEmailAsync(int memberId, string email)
        {
            MemberEntity entity = await context.Members
               .FirstOrDefaultAsync(m => m.Id == memberId);
            entity.Email = email;
            await context.SaveChangesAsync();
        }
    }
}
