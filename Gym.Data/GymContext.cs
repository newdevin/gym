﻿using System.Threading.Tasks;
using System.Transactions;
using Gym.Data.Entity;
using Gym.Service;
using Microsoft.EntityFrameworkCore;

namespace Gym.Data
{
    public class GymDbContext : DbContext, IUnitOfWork
    {
        private TransactionScope scope;

        public GymDbContext(DbContextOptions optionsBuilder) : base(optionsBuilder)
        {
            
        }

        public DbSet<MembershipEntity> Memberships { get; set; }
        public DbSet<MemberEntity> Members { get; set; }

        public void BeginTransaction()
        {
            scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
        }

        public void Rollback()
        {
            if (scope != null)
                scope.Dispose();
        }

        public async Task SaveAsync()
        {
            await base.SaveChangesAsync();
            if (scope != null)
            {
                scope.Complete();
                scope.Dispose();
            }
        }
    }
}
