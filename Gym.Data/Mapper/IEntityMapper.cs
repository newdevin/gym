﻿using System.Collections.Generic;
using System.Text;

namespace Gym.Data.Mapper
{
    public interface IEntityMapper
    {
        T Map<T>(object obj);
    }

}
