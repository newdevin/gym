﻿using AutoMapper;
using Gym.Domain;

namespace Gym.Data.Entity
{
    public class MembershipProfile : Profile
    {
        public MembershipProfile()
        {
            CreateMap<MembershipEntity, Membership>()
                .ConstructUsing(x => new Membership(x.Id, x.Name));

            CreateMap<Membership, MembershipEntity>();

            CreateMap<MemberEntity, Member>()
                .ConstructUsing(m =>
                     new Member(m.Id, m.FirstName, m.LastName, m.Email,
                       AutoMapper.Mapper.Map<Membership>(m.Membership),
                       m.RegisteredDateTime));

            CreateMap<string, Email>()
                .ConstructUsing(s => new Email(s));

            CreateMap<Email, string>()
                .ConstructUsing(e => e.email);
            //CreateMap<Member, MemberEntity>();
            //    .ForMember(dest => dest.Email, (opt) => opt.MapFrom(src => src.Email.email));

        }
    }
}
