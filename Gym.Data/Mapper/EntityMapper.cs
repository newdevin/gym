﻿using System;

namespace Gym.Data.Mapper
{
    public class EntityMapper : IEntityMapper
    {
        public T Map<T>(object obj)
        {
            return AutoMapper.Mapper.Map<T>(obj);
        }
    }

}
