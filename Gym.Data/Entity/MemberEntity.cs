﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gym.Data.Entity
{
    [Table("Member")]
    public class MemberEntity
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public DateTime RegisteredDateTime { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public int MembershipId { get; set; }
        public MembershipEntity Membership { get; set; }
    }
}
