﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gym.Data.Entity
{
    [Table("Membership")]
    public class MembershipEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
