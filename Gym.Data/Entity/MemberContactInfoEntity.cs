﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gym.Data.Entity
{
    public class MemberContactInfoEntity
    {
        public int Id { get; set; }
        public string FirstName { get; }
        public string LastName { get; }
        public string Email { get; set; }
        public MemberContactInfoEntity contactInfo { get; set; }
        public MembershipEntity Membership { get; set; }
        
    }
}
