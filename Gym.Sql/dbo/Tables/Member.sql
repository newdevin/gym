﻿CREATE TABLE [dbo].[Member]
(
	[Id] INT NOT NULL IDENTITY (1,1) CONSTRAINT [Pk_Member_Id] PRIMARY KEY,
	[FirstName] NVARCHAR(32) NOT NULL,
	[LastName] NVARCHAR(32) NOT NULL,
	[Email] NVARCHAR(32) NOT NULL CONSTRAINT [Uq_Member_Email] UNIQUE,
	[MembershipId] INT ,
	[RegisteredDateTime] Date NOT NULL,

)
