﻿CREATE TABLE [dbo].[MemberContactInfo]
(
	[Id] INT NOT NULL IDENTITY(1,1) CONSTRAINT [Pk_MemberContactInfo_Id] PRIMARY KEY,
	[MemberId] INT NOT NULL CONSTRAINT [Fk_MemberContactInfo_MemberId_Member_Id] FOREIGN KEY REFERENCES dbo.Member(Id),
	[MobileNumber] NVARCHAR(16),
	[PrimaryPhoneNumber] NVARCHAR(16),
	[SecondaryPhoneNumber] NVARCHAR(16),
	[FirstLineOfAddress] NVARCHAR(32) NOT NULL,
	[SecondLineOfAddress] NVARCHAR(322) NOT NULL,
	[ThirdLineOfAddress] NVARCHAR(32) NOT NULL,
	[PostCode] NVARCHAR(322) NOT NULL,
	[Country] NVARCHAR(32) NOT NULL,
)
