﻿namespace Gym.Domain
{
    public class MemberContactInfo
    {
        public string MobileNumber { get; }
        public string PrimaryPhoneNumber { get; }
        public string SecondaryPhoneNumber { get; }
        public string FirstLineOfAddress { get; }
        public string SecondLineOfAddress { get; }
        public string ThirdLineOfAddress { get; }
        public string PostCode { get; }
        public string Country { get; }



        public MemberContactInfo(string mobileNumber, string primaryPhoneNumber, string secondaryPhoneNumber, string firstLineOfAddress,
                                 string secondLineOfAddress, string thirdLineOfAddress, string postCode, string country)
        {
            SecondaryPhoneNumber = secondaryPhoneNumber;
            PrimaryPhoneNumber = primaryPhoneNumber;
            MobileNumber = mobileNumber;
            FirstLineOfAddress = firstLineOfAddress;
            SecondaryPhoneNumber = secondaryPhoneNumber;
            ThirdLineOfAddress = thirdLineOfAddress;
            PostCode = postCode;
            Country = country;

        }
    }
}