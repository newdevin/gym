﻿namespace Gym.Domain
{
    public abstract class Entity
    {
        public virtual int Id { get; protected set; }

        public override bool Equals(object obj)
        {
            var other = obj as Entity ;
            
            if (other is null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            if (Id == 0 || other.Id == 0)
                return false;

            return Id == other.Id;

        }

        public override int GetHashCode()
        {
            return Id;
        }
    }
}
