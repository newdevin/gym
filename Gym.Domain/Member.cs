﻿using System;

namespace Gym.Domain
{

    public class Member : Entity
    {
        

        public string FirstName { get; }
        public string LastName { get; }
        public Email Email { get; private set; }
        public DateTime RegisteredDate { get; }
        public MemberContactInfo contactInfo { get; private set; }
        public Membership Membership { get; private set; }

        public Member(int id, string firstName, string lastName,
            string email, Membership membership, DateTime registeredDate)
        {
            Membership = membership;
            Email = new Email(email);
            LastName = lastName;
            base.Id = id;
            FirstName = firstName;
            RegisteredDate = registeredDate;
        }

        public void UpdateMembership(Membership membership)
        {
            Membership = membership;
        }

        public void UpdateContactInfo(string mobileNumber, string primaryPhoneNumber, string secondaryPhoneNumber, string firstLineOfAddress,
                                 string secondLineOfAddress, string thirdLineOfAddress, string postCode, string country)
        {
            contactInfo = new MemberContactInfo(mobileNumber, primaryPhoneNumber, secondaryPhoneNumber, firstLineOfAddress,
                                 secondLineOfAddress, thirdLineOfAddress, postCode, country);

        }

        public void UpdateEmail(string newEmail)
        {
            Email = new Email(newEmail);
        }

    }


}
