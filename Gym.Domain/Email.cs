﻿namespace Gym.Domain
{
    public class Email
    {
        public string email { get; private set; }

        public Email(string email)
        {
            ThrowIfNotValidEmail(email);
            this.email = email;
        }

        private void ThrowIfNotValidEmail(string email)
        {
            if (!email.Contains("@"))
                throw new InvalidEmailException(nameof(email));

        }
    }
}
