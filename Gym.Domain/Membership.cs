﻿using System;

namespace Gym.Domain
{
    public class Membership : Entity
    {
        public string Name { get; private set; }

        public Membership(int id, string name)
        {
            base.Id = id;
            Name = name;
        }


    }
}
